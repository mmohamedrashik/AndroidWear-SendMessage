package guy.droid.com.wearnotification;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.List;

/**
 * Created by admin on 9/17/2016.
 */
public class WearListCallListenerService  extends WearableListenerService{

    public static String  WEAR_PATH = "/from-wear";
    int i = 0;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getApplicationContext(),"WEAR START",Toast.LENGTH_LONG).show();
        return START_NOT_STICKY;

    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);
        String message = new String(messageEvent.getData());
        if(messageEvent.getPath().equals(WEAR_PATH))
        {  i++;
           // Toast.makeText(getApplicationContext(),""+message,Toast.LENGTH_SHORT).show();
            Log.d("----",i+"");
        }
    }

}